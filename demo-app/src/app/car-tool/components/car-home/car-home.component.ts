import { Component, OnInit } from '@angular/core';

import { DataTableConfig } from '../../../shared/models/data-table-config';
import { Car } from '../../../car-tool/models/car';

@Component({
  selector: 'app-car-home',
  templateUrl: './car-home.component.html',
  styleUrls: ['./car-home.component.css']
})
export class CarHomeComponent implements OnInit {

  carsConfig: DataTableConfig = {
    columns: [
      { caption: 'Id', field: 'id', order: 1 },
      { caption: 'Make', field: 'make', order: 2 },
      { caption: 'Model', field: 'model', order: 3 },
      { caption: 'Year', field: 'year', order: 4 },
      { caption: 'Color', field: 'color', order: 5 },
      { caption: 'Price', field: 'price', order: 6 },
    ]
  };

  cars: Car[] = [
    { id: 1, make: 'Ford', model: 'T', year: 1917, color: 'black', price: 350 },
    { id: 2, make: 'Tesla', model: 'S', year: 2017, color: 'blue', price: 120000 },
    { id: 3, make: 'Ford', model: 'T', year: 1917, color: 'black', price: 350 },
    { id: 4, make: 'Tesla', model: 'S', year: 2017, color: 'blue', price: 120000 },
    { id: 5, make: 'Ford', model: 'T', year: 1917, color: 'black', price: 350 },
    { id: 6, make: 'Tesla', model: 'S', year: 2017, color: 'blue', price: 120000 },
    { id: 7, make: 'Ford', model: 'T', year: 1917, color: 'black', price: 350 },
    { id: 8, make: 'Tesla', model: 'S', year: 2017, color: 'blue', price: 120000 },
    { id: 9, make: 'Ford', model: 'T', year: 1917, color: 'black', price: 350 },
    { id: 10, make: 'Tesla', model: 'S', year: 2017, color: 'blue', price: 120000 },
    { id: 11, make: 'Ford', model: 'T', year: 1917, color: 'black', price: 350 },
    { id: 12, make: 'Tesla', model: 'S', year: 2017, color: 'blue', price: 120000 },
  ];

  constructor() { }

  ngOnInit() {
  }

}
