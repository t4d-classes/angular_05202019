import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';

import { CarHomeComponent } from './components/car-home/car-home.component';

@NgModule({
  declarations: [CarHomeComponent],
  imports: [
    CommonModule, SharedModule,
  ],
  exports: [ CarHomeComponent ],
})
export class CarToolModule { }
