export interface DataTableColumn {
  caption: string;
  field: string;
  order: number;
}

export interface DataTableConfig {
  columns: DataTableColumn[];
}
