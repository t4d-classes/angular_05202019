import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataTableComponent } from './components/data-table/data-table.component';
import { DataHeaderComponent } from './components/data-header/data-header.component';
import { DataRowComponent } from './components/data-row/data-row.component';
import { PageableTableComponent } from './components/pageable-table/pageable-table.component';

@NgModule({
  declarations: [
    DataTableComponent,
    DataHeaderComponent,
    DataRowComponent,
    PageableTableComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    DataTableComponent,
    PageableTableComponent,
  ]
})
export class SharedModule { }
