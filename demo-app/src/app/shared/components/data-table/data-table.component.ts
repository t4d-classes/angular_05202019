import { Component, OnInit, Input, EventEmitter } from '@angular/core';

import { DataTableConfig, DataTableColumn } from '../../models/data-table-config';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {

  private originalConfig: DataTableConfig;
  private processedConfig: DataTableConfig;
  private originalData: object[] = [];
  private currentData: object[] = [];

  get config() {
    return this.processedConfig;
  }

  @Input()
  set config(value: DataTableConfig) {
    this.originalConfig = value;
    this.processedConfig = {
      ...this.originalConfig,
      columns: this.originalConfig.columns.concat().sort((a: DataTableColumn, b: DataTableColumn) => {
        if (a.order < b.order) {
          return -1;
        }
        if (a.order > b.order) {
          return 1;
        }
        return 0;
      }),
    };
  }

  refreshData = new EventEmitter<object[]>();

  get data() {
    return this.currentData;
  }

  @Input()
  set data(value: object[]) {
    if (value !== this.originalData) {
      this.originalData = value;
      this.currentData = value;
      this.refreshData.emit(this.originalData);
    }
  }

  setCurrentData(data: object[]) {
    this.currentData = data;
  }

  constructor() { }

  ngOnInit() {
  }

  get captions() {
    return this.processedConfig.columns.map(c => c.caption);
  }

  get fields() {
    return this.processedConfig.columns.map(c => c.field);
  }

}
