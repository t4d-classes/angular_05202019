import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'tr[data-row]',
  templateUrl: './data-row.component.html',
  styleUrls: ['./data-row.component.css']
})
export class DataRowComponent implements OnInit {

  @Input()
  fields: string[] = [];

  @Input()
  data: object = {};

  constructor() { }

  ngOnInit() {
  }

}
