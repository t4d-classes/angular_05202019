import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'tr[data-header]',
  templateUrl: './data-header.component.html',
  styleUrls: ['./data-header.component.css']
})
export class DataHeaderComponent implements OnInit {

  @Input()
  captions: string[] = [];

  constructor() { }

  ngOnInit() {
  }

}
