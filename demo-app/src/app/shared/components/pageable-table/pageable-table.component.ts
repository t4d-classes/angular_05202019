import {
  Component, OnInit, Input, ContentChild,
  AfterContentInit, AfterContentChecked } from '@angular/core';

import { DataTableComponent } from '../data-table/data-table.component';

@Component({
  selector: 'app-pageable-table',
  templateUrl: './pageable-table.component.html',
  styleUrls: ['./pageable-table.component.css']
})
export class PageableTableComponent implements OnInit, AfterContentInit, AfterContentChecked {

  @ContentChild(DataTableComponent)
  dataTable: DataTableComponent;

  originalData: object[];

  @Input()
  pageLength = 10;

  // 1-based paging
  currentPage = 1;
  numOfPages = 0;

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentInit() {
    this.initDataTable(this.dataTable.data);
    this.dataTable.refreshData.subscribe(this.initDataTable.bind(this));
  }

  ngAfterContentChecked() {
    this.dataTable.setCurrentData(
    this.originalData.slice(this.startIndex, this.endIndex));
  }

  initDataTable(data: object[]) {
    this.originalData = data;
    this.numOfPages = Math.ceil(
      this.originalData.length / this.pageLength);
  }

  get startIndex() {
    return (this.currentPage - 1) * this.pageLength;
  }

  get endIndex() {
    return this.currentPage * this.pageLength;
  }

  gotoFirst() {
    this.currentPage = 1;
  }

  gotoPrevious() {
    this.currentPage = Math.max(this.currentPage - 1, 1);
  }

  gotoNext() {
    this.currentPage = Math.min(this.currentPage + 1, this.numOfPages);
  }

  gotoLast() {
    this.currentPage = this.numOfPages;
  }

}
