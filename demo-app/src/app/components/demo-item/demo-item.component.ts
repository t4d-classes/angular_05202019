import { Component, OnInit, Input, DoCheck } from '@angular/core';

@Component({
  selector: 'app-demo-item',
  templateUrl: './demo-item.component.html',
  styleUrls: ['./demo-item.component.css']
})
export class DemoItemComponent implements OnInit, DoCheck {

  @Input()
  item = '';

  constructor() { }

  ngOnInit() {
  }

  ngDoCheck() {
    console.log('running change detection');
  }

}
