import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-unordered-list',
  templateUrl: './unordered-list.component.html',
  styleUrls: ['./unordered-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnorderedListComponent implements OnInit {

  @Input()
  items: string[] = [];

  constructor() { }

  ngOnInit() {
  }

}
