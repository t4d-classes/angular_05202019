import {
  Component, OnInit, ViewChild, AfterViewInit, Directive,
  Output, EventEmitter,
} from '@angular/core';
import { NgModel, FormControl, Validators, NG_VALIDATORS} from '@angular/forms';

const myRequired = (c: FormControl) => {

  if (c.value == null || String(c.value).length === 0) {
    return {
      myRequired: true,
    };
  }

  return null;
};

@Directive({
  selector: '[ngModel][myRequired]',
  providers: [
    { provide: NG_VALIDATORS, useValue: myRequired, multi: true },
  ]

})
export class MyRequiredValidatorDirective { }

@Component({
  selector: 'app-color-form',
  templateUrl: './color-form.component.html',
  styleUrls: ['./color-form.component.css']
})
export class ColorFormComponent implements OnInit {

  @Output()
  submitColor = new EventEmitter<string>();

  @ViewChild(NgModel)
  newColorInput;

  newHexCodeInput = new FormControl('', {
    validators: [ myRequired ],
  });

  newColor = '';
  newHexCode = '';

  constructor() { }

  ngOnInit() {
  }

  saveColor() {
    this.newHexCode = this.newHexCodeInput.value;

    this.submitColor.emit(this.newColor);
  }

  // ngAfterViewInit() {
  //   console.dir(this.newColorModel);
  // }

}
