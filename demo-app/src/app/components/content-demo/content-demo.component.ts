import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-content-demo',
  templateUrl: './content-demo.component.html',
  styleUrls: ['./content-demo.component.css'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class ContentDemoComponent implements OnInit {

  title = 'Content Demo';

  constructor() { }

  ngOnInit() {
  }

}
