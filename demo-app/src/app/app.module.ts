import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CarToolModule } from './car-tool/car-tool.module';

import { AppComponent } from './app.component';
import { ToolHeaderComponent } from './components/tool-header/tool-header.component';
import { UnorderedListComponent } from './components/unordered-list/unordered-list.component';
import { ColorFormComponent, MyRequiredValidatorDirective } from './components/color-form/color-form.component';
import { DemoItemComponent } from './components/demo-item/demo-item.component';
import { ContentDemoComponent } from './components/content-demo/content-demo.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolHeaderComponent,
    UnorderedListComponent,
    ColorFormComponent,
    MyRequiredValidatorDirective,
    DemoItemComponent,
    ContentDemoComponent,
  ],
  imports: [
    BrowserModule, ReactiveFormsModule, FormsModule, CarToolModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
