import { Component, ViewEncapsulation } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class AppComponent {

  title = 'App Component';

  // title = 'Color Tool';

  // colors = [ 'green', 'black', 'red', 'purple', 'blue' ];

  // sports = [
  //   { id: 1, name: 'soccer' },
  //   { id: 2, name: 'football' },
  //   { id: 3, name: 'basketball' },
  //   { id: 4, name: 'putt-putt'},
  // ];

  // constructor() {}

  // replaceSports() {
  //   console.log('replace clicked');
  //   this.sports = [
  //     { id: 1, name: 'soccer' },
  //     { id: 2, name: 'football' },
  //     { id: 3, name: 'basketball' },
  //     { id: 4, name: 'putt-putt'},
  //   ];
  // }

  // trackSportName(index: number, sport: { id: number, name: string }) {
  //   return sport.name;
  // }


  // appendColor(color: string) {
  //   this.colors = this.colors.concat(color);
  //   // this.colors = [ ...this.colors, color ];

  //   // const [ first, ...other ] = this.colors;

  //   // this.colors.push(color);
  // }

}
