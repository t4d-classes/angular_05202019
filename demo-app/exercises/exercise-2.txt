Exercise 2

https://gitlab.com/t4d-classes/angular_05202019

1. Create a new module named Shared.

2. Move the ToolHeader component to the Shared module.

3. Create a component named DataTable in the Shared module. The component should have the following features:

$ ng g component shared/components/data-table --export

 - The ability to customize the number of columns
 - For each column, specify the column header and the object property to populate the column with.
 - The table, header row, and data row should each be their own component:

   <the-table>
    <the-header-row>
    <the-data-row> (one instance for each row of data)
   </the-table>

4. Create a new module named CarTool.

5. Add a component named CarHome in the CarTool module.

6. Code the CarHome component to use the ToolHeader component to display "Car Tool" at the top of the component.

7. Under the ToolHeader component in CarHome, display a table of cars. The list of cars objects should be stored within CarHome and each car should have the following fields:

name: id, type: number, header: Id
name: make, type: string, header: Make
name: model, type: string, header: Model
name: year, type: number, header: Year
name: color, type: string, header: Color
name: price, type: number, header: Price

8. Use the CarHome component in the AppComponent.

9. Run the application and ensure the "Car Tool" header and a data table of cars is displayed.
