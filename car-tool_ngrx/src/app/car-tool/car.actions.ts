import { Action } from '@ngrx/store';

import { Car } from './models/car';

export enum CarActionTypes {
  REFRESH_CARS_REQUEST = '[Car] Refresh Cars Request',
  REFRESH_CARS_DONE = '[Car] Refresh Cars Done',
  APPEND_CAR_REQUEST = '[Car] Append Car Request',
  APPEND_CAR_DONE = '[Car] Append Car Done',
  REPLACE_CAR = '[Car] Replace Car',
  DELETE_CAR = '[Car] Delete Car',
  EDIT_CAR = '[Car] Edit Car',
  CANCEL_CAR = '[Car] Cancel Car',
}

export class RefreshCarsRequestAction implements Action {
  type = CarActionTypes.REFRESH_CARS_REQUEST;
  constructor() { }
}

export class RefreshCarsDoneAction implements Action {
  type = CarActionTypes.REFRESH_CARS_DONE;
  constructor(public payload: Car[]) { }
}

export class AppendCarRequestAction implements Action {
  type = CarActionTypes.APPEND_CAR_REQUEST;
  constructor(public payload: Car) { }
}

export class AppendCarDoneAction implements Action {
  type = CarActionTypes.APPEND_CAR_DONE;
  constructor(public payload: Car) { }
}

export class ReplaceCarAction implements Action {
  type = CarActionTypes.REPLACE_CAR;
  constructor(public payload: Car) { }
}

export class DeleteCarAction implements Action {
  type = CarActionTypes.DELETE_CAR;
  constructor(public payload: number) { }
}

export class EditCarAction implements Action {
  type = CarActionTypes.EDIT_CAR;
  constructor(public payload: number) { }
}

export class CancelCarAction implements Action {
  type = CarActionTypes.CANCEL_CAR;
  constructor() { }
}

export type CarActionsUnion = RefreshCarsRequestAction | RefreshCarsDoneAction |
  AppendCarRequestAction | AppendCarDoneAction |
  ReplaceCarAction | DeleteCarAction |
  EditCarAction | CancelCarAction;
