import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';

import { Car } from './models/car';
import { CarActionTypes, CarActionsUnion,
  RefreshCarsDoneAction, AppendCarRequestAction, RefreshCarsRequestAction } from './car.actions';

@Injectable({
  providedIn: 'root',
})
export class CarEffects {

  constructor(
    private httpClient: HttpClient,
    private actions$: Actions<CarActionsUnion>,
  ) { }

  @Effect()
  refresh$: Observable<RefreshCarsDoneAction> = this.actions$.pipe(
    ofType(CarActionTypes.REFRESH_CARS_REQUEST),
    switchMap(() => {
      return this.httpClient
        .get<Car[]>('http://localhost:4250/cars')
        .pipe(map(cars => new RefreshCarsDoneAction(cars)));
    }),
  );

  @Effect()
  append$: Observable<RefreshCarsRequestAction> = this.actions$.pipe(
    ofType(CarActionTypes.APPEND_CAR_REQUEST),
    switchMap((action: AppendCarRequestAction) => {
      return this.httpClient
        .post<Car>(
          'http://localhost:4250/cars',
          action.payload,
        )
        .pipe(map(() => new RefreshCarsRequestAction()));
    }),
  );

}