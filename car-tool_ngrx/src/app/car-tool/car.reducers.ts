import { CarActionTypes, CarActionsUnion, DeleteCarAction, ReplaceCarAction, EditCarAction, RefreshCarsDoneAction  } from './car.actions';

import { Car } from './models/car';

export const carsReducer = (state: Car[] = [], action: CarActionsUnion) => {

  switch (action.type) {
    case CarActionTypes.REFRESH_CARS_DONE:
      return (action as RefreshCarsDoneAction).payload;
    case CarActionTypes.REPLACE_CAR:
      const carToReplace = (action as ReplaceCarAction).payload as Car;
      const newCars = state.concat();
      newCars[newCars.findIndex(c => c.id === carToReplace.id)] = carToReplace;
      return newCars;
    case CarActionTypes.DELETE_CAR:
      return state.filter(c => c.id !== (action as DeleteCarAction).payload);
    default:
      return state;
  }

};

export const editCarIdReducer = (state = -1, action: CarActionsUnion) => {

  switch (action.type) {
    case CarActionTypes.EDIT_CAR:
      return (action as EditCarAction).payload;
    case CarActionTypes.REFRESH_CARS_DONE:
    case CarActionTypes.APPEND_CAR_DONE:
    case CarActionTypes.REPLACE_CAR:
    case CarActionTypes.DELETE_CAR:
    case CarActionTypes.CANCEL_CAR:
      return -1;
    default:
      return state;
  }

};
