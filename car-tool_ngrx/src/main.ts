import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

// import { Observable, Observer, of } from 'rxjs';
// import { take, catchError, map } from 'rxjs/operators';

// const nums$ = Observable.create(
//   (observer: Observer<number>) => {

//     console.log('called subscribe');

//     let counter = 0;

//     const handle = setInterval(() => {

//       if (counter === 3) {
//         observer.error('too many numbers');
//       }

//       if (observer.closed) {
//         clearInterval(handle);
//         return;
//       }

//       console.log('inside set interval', counter);
//       observer.next(counter++);

//     }, 500);

//     setTimeout(() => {
//       clearInterval(handle);
//       observer.complete();
//     }, 5000);

//   }
// );

// nums$.pipe(
//   take(5),
//   catchError(() => {
//     return of(100, 101, 102);
//   }),
//   map((n: number) => n * 3),
// ).subscribe(num => {
//   console.log(num);
// }, err => {
//   console.log(err);
//   console.log('handled observable error');
// }, () => {
//   console.log('completed');
// });

// nums$.subscribe(num => {
//   console.log(num);
// });

// const p = new Promise((resolve) => {

//   console.log('called promise');

//   setTimeout(() => {
//     console.log(0);
//     resolve(0);
//   }, 500);

// });

// p.then(result => console.log(result));
// p.then(result => console.log(result));

