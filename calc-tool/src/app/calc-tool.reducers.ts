import { CalcToolActionTypes, CalcActionsUnion } from './calc-tool.actions';

export const resultReducer = (state = 0, action: CalcActionsUnion) => {

  switch (action.type) {
    case CalcToolActionTypes.ADD:
      return state + action.payload;
    case CalcToolActionTypes.SUBTRACT:
      return state - action.payload;
    default:
      return state;
  }

};