import { Action } from '@ngrx/store';

export enum CalcToolActionTypes {
  ADD = '[CalcTool] ADD',
  SUBTRACT = '[CalcTool] SUBTRACT',
}

export class AddAction implements Action {
  type = CalcToolActionTypes.ADD;
  // public payload: number;
  // constructor(payload: number) {
  //   this.payload = payload;
  // }
  constructor(public payload: number) { }
}

export class SubtractAction implements Action {
  type = CalcToolActionTypes.SUBTRACT;
  constructor(public payload: number) { }
}

export type CalcActionsUnion = AddAction | SubtractAction;

