import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Store, select } from '@ngrx/store';

import { CalcToolState } from './calc-tool-state';
import { AddAction, SubtractAction } from './calc-tool.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  numInput = new FormControl(0);

  result$ = this.store.pipe(select('result'));

  constructor(private store: Store<CalcToolState>) { }

  doAdd() {
    this.store.dispatch(new AddAction(this.numInput.value));
  }

  doSubtract() {
    this.store.dispatch(new SubtractAction(this.numInput.value));
  }
}

