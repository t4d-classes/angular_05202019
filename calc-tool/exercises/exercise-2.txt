Exercise 2.

1. Display the history of mathematical operations under the buttons. For the history show the name of the operation and value of the operations (not the results). As you perform each operation the list should grow.

2. Add a clear button. When the button is clicked the result is set to zero and history is erased.

Hint: The history should be implemented as a second reducer on a history property on the state.

3. Ensure it works.